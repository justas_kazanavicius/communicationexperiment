﻿using Data;
using System.Net.Http.Json;

namespace HttpClient
{
    public class Client : IDisposable
    {
        private readonly System.Net.Http.HttpClient _client;

        public Client(int port)
        {
            _client = new System.Net.Http.HttpClient();
            _client.BaseAddress = new Uri($"http://localhost:{port}");
        }

        public async Task<TestModelOnlyText> GetTextOnly10()
        {
            return await _client.GetFromJsonAsync<TestModelOnlyText>("api/Test/GetTextOnly10") ?? throw new Exception("Null response");
        }

        public async Task<TestModelOnlyText> GetTextOnly1000()
        {
            return await _client.GetFromJsonAsync<TestModelOnlyText>("api/Test/GetTextOnly1000") ?? throw new Exception("Null response");
        }

        public async Task<TestModelOnlyText> GetTextOnly100000()
        {
            return await _client.GetFromJsonAsync<TestModelOnlyText>("api/Test/GetTextOnly100000") ?? throw new Exception("Null response");
        }

        public async Task<TestModelOnlyText> GetTextOnly1000000()
        {
            return await _client.GetFromJsonAsync<TestModelOnlyText>("api/Test/GetTextOnly1000000") ?? throw new Exception("Null response");
        }

        public async Task<TestModelAllTypes> GetAllTypes()
        {
            return await _client.GetFromJsonAsync<TestModelAllTypes>("api/Test/GetAllTypes") ?? throw new Exception("Null response");
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList10()
        {
            return await _client.GetFromJsonAsync<List<TestModelAllTypes>>("api/Test/GetAllTypesList10") ?? throw new Exception("Null response");
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList100()
        {
            return await _client.GetFromJsonAsync<List<TestModelAllTypes>>("api/Test/GetAllTypesList100") ?? throw new Exception("Null response");
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList1000()
        {
            return await _client.GetFromJsonAsync<List<TestModelAllTypes>>("api/Test/GetAllTypesList1000") ?? throw new Exception("Null response");
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
