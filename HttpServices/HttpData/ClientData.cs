﻿using Data;
using HttpClient;

namespace HttpData
{
    public class ClientData : ITestData, IDisposable
    {
        private readonly Client _client;

        public async Task<TestModelOnlyText> TestModelOnlyTextType10() => await _client.GetTextOnly10();

        public async Task<TestModelOnlyText> TestModelOnlyTextType1000() => await _client.GetTextOnly1000();

        public async Task<TestModelOnlyText> TestModelOnlyTextType100000() => await _client.GetTextOnly100000();

        public async Task<TestModelOnlyText> TestModelOnlyTextType1000000() => await _client.GetTextOnly1000000();

        public async Task<TestModelAllTypes> TestModelAllTypesType() => await _client.GetAllTypes();

        public async Task<List<TestModelAllTypes>> ListTestModelAllTypesType10() => await _client.GetAllTypesList10();

        public async Task<List<TestModelAllTypes>> ListTestModelAllTypesType100() => await _client.GetAllTypesList100();

        public async Task<List<TestModelAllTypes>> ListTestModelAllTypesType1000() => await _client.GetAllTypesList1000();

        public ClientData(int port)
        {
            _client = new Client(port);
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
