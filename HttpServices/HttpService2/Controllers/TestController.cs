using Data;
using Microsoft.AspNetCore.Mvc;

namespace HttpService2.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly ITestData _testData;

        public TestController(ITestData testData)
        {
            _testData = testData;
        }

        [HttpGet]
        [ActionName("GetTextOnly10")]
        public async Task<TestModelOnlyText> GetTextOnly10()
        {
            return await _testData.TestModelOnlyTextType10();
        }

        [HttpGet]
        [ActionName("GetTextOnly1000")]
        public async Task<TestModelOnlyText> GetTextOnly1000()
        {
            return await _testData.TestModelOnlyTextType1000();
        }

        [HttpGet]
        [ActionName("GetTextOnly100000")]
        public async Task<TestModelOnlyText> GetTextOnly100000()
        {
            return await _testData.TestModelOnlyTextType100000();
        }

        [HttpGet]
        [ActionName("GetTextOnly1000000")]
        public async Task<TestModelOnlyText> GetTextOnly1000000()
        {
            return await _testData.TestModelOnlyTextType1000000();
        }

        [HttpGet]
        [ActionName("GetAllTypes")]
        public async Task<TestModelAllTypes> GetAllTypes()
        {
            return await _testData.TestModelAllTypesType();
        }

        [HttpGet]
        [ActionName("GetAllTypesList10")]
        public async Task<List<TestModelAllTypes>> GetAllTypesList10()
        {
            return await _testData.ListTestModelAllTypesType10();
        }

        [HttpGet]
        [ActionName("GetAllTypesList100")]
        public async Task<List<TestModelAllTypes>> GetAllTypesList100()
        {
            return await _testData.ListTestModelAllTypesType100();
        }

        [HttpGet]
        [ActionName("GetAllTypesList1000")]
        public async Task<List<TestModelAllTypes>> GetAllTypesList1000()
        {
            return await _testData.ListTestModelAllTypesType1000();
        }
    }
}
