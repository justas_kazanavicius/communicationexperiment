﻿using Newtonsoft.Json;
using ResultsCalculations.Models;

namespace ResultsCalculations.Help
{
    public class Parser
    {
        public static Root GetData(string file)
        {
            var content = File.ReadAllText(file);
            return JsonConvert.DeserializeObject<Root>(content);
        }
    }
}
