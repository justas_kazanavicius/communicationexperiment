using CsvHelper;
using Newtonsoft.Json;
using ResultsCalculations.Help;
using ResultsCalculations.Models;
using System.Globalization;
using System.Text.RegularExpressions;
using Xunit.Abstractions;

namespace ResultsCalculations
{
    public class ParserTests
    {
        private readonly ITestOutputHelper _output;

        public ParserTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData("http")]
        [InlineData("Rabbit")]
        [InlineData("Kafka")]
        [InlineData("Grpc")]
        [InlineData("Graphql")]
        public void GetRpsData(string protocol)
        {
            var stats = GetStats(protocol).Select(x => x.Rps);

            using (var writer = new StreamWriter($"../../../Results/{protocol}_RPS.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(stats);
            }
        }

        [Theory]
        [InlineData("http")]
        [InlineData("Rabbit")]
        [InlineData("Kafka")]
        [InlineData("Grpc")]
        [InlineData("Graphql")]
        public void GetRpsDataWithFailedAlso(string protocol)
        {
            var stats = GetStats(protocol).Select(x => x.Rps + x.RpsFailed).ToList();

            var average = stats.Average();
            var averagetill140 = stats.Take(39).Average();
            var averageafter140 = stats.Skip(39).Average();

            stats.Add(average);
            stats.Add(averagetill140);
            stats.Add(averageafter140);

            using (var writer = new StreamWriter($"../../../Results/{protocol}_RPS_both.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(stats);
            }
        }

        [Theory]
        [InlineData("http")]
        public void GetTimeLine(string protocol)
        {
            var stats = GetStats(protocol).Select(x => new OnlyDurationStats { Duration = x.Duration }).ToList();

            using (var writer = new StreamWriter($"../../../Results/duration.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(stats);
            }
        }

        private static IEnumerable<ThroughputStats> GetStats(string protocol)
        {
            var data = Parser.GetData($"Data/{protocol}.json");

            return data.TimeLineHistory.Select(timeLineHistory =>
                new ThroughputStats
                {
                    Duration = timeLineHistory.Duration,
                    Request = timeLineHistory.ScenarioStats.Single().StepStats.Single().Ok.Request.Count,
                    Rps = timeLineHistory.ScenarioStats.Single().StepStats.Single().Ok.Request.RPS,
                    RpsFailed = timeLineHistory.ScenarioStats.Single().StepStats.Single().Fail.Request.RPS
                });
        }

        [Fact]
        public void ParseResults()
        {
            var path = @"D:\_Git\PhD_\Conferences And Artickles\2022 Communication\Results\Bomber\_Last";

            var files = Directory.GetFiles(path, "*.html", SearchOption.AllDirectories);

            var dic = new Dictionary<string, List<List<double>>>();

            foreach (var file in files)
            {
                var readAllText = File.ReadAllText(file);

                var pattern = @"Duration.:.\d\d:\d\d:\d\d\.\d\d\d\d\d\d\d";
                var regEx = new Regex(pattern);
                var sanitized = regEx.Replace(readAllText, match => match.Value.Substring(0, 19));

                File.WriteAllText(file, sanitized);

                string last = Between(sanitized, "const viewModel = ", "initApp('#app', viewModel)").TrimEnd();

                last = last.Remove(last.Length - 1, 1);

                var data = JsonConvert.DeserializeObject<Root>(last);

                var rps = data.TimeLineHistory.Select(timeLineHistory => timeLineHistory.ScenarioStats.Single().StepStats.Single().Ok.Request.RPS).ToList();

                if (rps.Count() != 60)
                {
                    for (int i = 0; i < 60 - rps.Count; i++)
                    {
                        rps.Add(0);
                    }
                }

                var strings = file.Split('\\');

                var newName = strings[8] + " " + strings[9];

                if (dic.ContainsKey(newName))
                {
                    var list = dic[newName];
                    list.Add(rps);

                    dic[newName] = list;
                }
                else
                {
                    dic.Add(newName, new List<List<double>> { rps });
                }
            }

            using (var writer = new StreamWriter($"../../../Results/All_RPS.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.CurrentCulture))
            {
                foreach (var item in dic)
                {
                    csv.WriteField(item.Key);

                    foreach (var value in Average(item.Value))
                    {
                        csv.WriteField(value);
                    }

                    csv.NextRecord();
                }
            }
        }

        public string Between(string STR, string FirstString, string LastString)
        {
            string FinalString;
            int Pos1 = STR.IndexOf(FirstString) + FirstString.Length;
            int Pos2 = STR.IndexOf(LastString);
            FinalString = STR.Substring(Pos1, Pos2 - Pos1);
            return FinalString;
        }

        private List<double> Average(List<List<double>> values)
        {
            try
            {
                return values[0].Select((v, c) => values.Average(r => r[c])).Select(x => Math.Round(x, 2)).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
