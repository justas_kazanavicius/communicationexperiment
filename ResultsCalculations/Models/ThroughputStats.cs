namespace ResultsCalculations.Models
{
    public class ThroughputStats
    {
        public string Duration { get; set; }
        public int Request { get; set; }
        public double Rps { get; set; }
        public double RpsFailed { get; set; }
    }
}
