﻿using Newtonsoft.Json;

namespace ResultsCalculations.Models
{
    public class DataTransfer
    {
        public int MinBytes { get; set; }
        public int MeanBytes { get; set; }
        public int MaxBytes { get; set; }
        public int Percent50 { get; set; }
        public int Percent75 { get; set; }
        public int Percent95 { get; set; }
        public int Percent99 { get; set; }
        public double StdDev { get; set; }
        public int AllBytes { get; set; }
    }

    public class Fail
    {
        public Request Request { get; set; }
        public Latency Latency { get; set; }
        public DataTransfer DataTransfer { get; set; }
        public List<StatusCode> StatusCodes { get; set; }
    }

    public class FinalStats
    {
        public int RequestCount { get; set; }
        public int OkCount { get; set; }
        public int FailCount { get; set; }
        public int AllBytes { get; set; }
        public List<ScenarioStat> ScenarioStats { get; set; }
        public List<object> PluginStats { get; set; }
        public NodeInfo NodeInfo { get; set; }
        public TestInfo TestInfo { get; set; }
        public List<object> ReportFiles { get; set; }
        public string Duration { get; set; }
    }

    public class Hint
    {
        public string SourceName { get; set; }
        public string SourceType { get; set; }
        //public string Hint { get; set; }
    }

    public class Latency
    {
        public double MinMs { get; set; }
        public double MeanMs { get; set; }
        public double MaxMs { get; set; }
        public double Percent50 { get; set; }
        public double Percent75 { get; set; }
        public double Percent95 { get; set; }
        public double Percent99 { get; set; }
        public double StdDev { get; set; }
        public LatencyCount LatencyCount { get; set; }
    }

    public class LatencyCount
    {
        public int LessOrEq800 { get; set; }
        public int More800Less1200 { get; set; }
        public int MoreOrEq1200 { get; set; }
    }

    public class LoadSimulationStats
    {
        public string SimulationName { get; set; }
        public int Value { get; set; }
    }

    public class NodeInfo
    {
        public string MachineName { get; set; }
        public NodeType NodeType { get; set; }
        public int CurrentOperation { get; set; }
        public OS OS { get; set; }
        public string DotNetVersion { get; set; }
        public string Processor { get; set; }
        public int CoresCount { get; set; }
        public string NBomberVersion { get; set; }
    }

    public class NodeType
    {
        public string Case { get; set; }
    }

    public class Ok
    {
        public Request Request { get; set; }
        public Latency Latency { get; set; }
        public DataTransfer DataTransfer { get; set; }
        public List<object> StatusCodes { get; set; }
    }

    public class OS
    {
        public int Platform { get; set; }
        public string ServicePack { get; set; }
        public string Version { get; set; }
        public string VersionString { get; set; }
    }

    public class Request
    {
        public int Count { get; set; }
        public double RPS { get; set; }
    }

    public class Root
    {
        public FinalStats FinalStats { get; set; }
        public List<TimeLineHistory> TimeLineHistory { get; set; }
        public List<Hint> Hints { get; set; }
    }

    public class ScenarioStat
    {
        public string ScenarioName { get; set; }
        public int RequestCount { get; set; }
        public int OkCount { get; set; }
        public int FailCount { get; set; }
        public int AllBytes { get; set; }
        public List<StepStat> StepStats { get; set; }
        public LatencyCount LatencyCount { get; set; }
        public LoadSimulationStats LoadSimulationStats { get; set; }
        public List<StatusCode> StatusCodes { get; set; }
        public int CurrentOperation { get; set; }
        public string Duration { get; set; }
    }

    public class StatusCode
    {
        [JsonProperty("Count@")]
        public int Count { get; set; }

        //public int StatusCode { get; set; }
        public bool IsError { get; set; }

        public string Message { get; set; }
        //public int Count { get; set; }
    }

    public class StepStat
    {
        public string StepName { get; set; }
        public Ok Ok { get; set; }
        public Fail Fail { get; set; }
    }

    public class TestInfo
    {
        public string SessionId { get; set; }
        public string TestSuite { get; set; }
        public string TestName { get; set; }
    }

    public class TimeLineHistory
    {
        public List<ScenarioStat> ScenarioStats { get; set; }
        public string Duration { get; set; }
    }
}
