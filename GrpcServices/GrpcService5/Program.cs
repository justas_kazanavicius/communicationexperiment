using Data;
using GrpcData;
using GrpcService5;
using ProtoBuf.Grpc.Server;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSingleton<ITestData, TestData>();
builder.Services.AddSingleton<IGrpcClientData, GrpcClientData>();
builder.Services.AddCodeFirstGrpc();
var app = builder.Build();

app.MapGrpcService<GrpcTestService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client.");

app.Run();
