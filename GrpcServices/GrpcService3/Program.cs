using GrpcClient;
using GrpcData;
using GrpcService3;
using ProtoBuf.Grpc.Server;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IGrpcClientData>(new ClientData(5404));
builder.Services.AddCodeFirstGrpc();
var app = builder.Build();

app.MapGrpcService<GrpcTestService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client.");

app.Run();
