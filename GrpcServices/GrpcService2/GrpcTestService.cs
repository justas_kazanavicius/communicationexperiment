﻿using GrpcData;
using ProtoBuf.Grpc;

namespace GrpcService2
{
    public class GrpcTestService : IGrpcTestApiService
    {
        private readonly IGrpcClientData _testData;

        public GrpcTestService(IGrpcClientData testData)
        {
            _testData = testData;
        }

        public async Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType10(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.TestModelOnlyTextType10();
        }

        public async Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType1000(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.TestModelOnlyTextType1000();
        }

        public async Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType100000(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.TestModelOnlyTextType100000();
        }

        public async Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType1000000(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.TestModelOnlyTextType1000000();
        }

        public async Task<TestModelAllTypesReply> GetTestModelAllTypesType(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.TestModelAllTypesType();
        }

        public async Task<List<TestModelAllTypesReply>> GetListTestModelAllTypesType10(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.ListTestModelAllTypesType10();
        }

        public async Task<List<TestModelAllTypesReply>> GetListTestModelAllTypesType100(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.ListTestModelAllTypesType100();
        }

        public async Task<List<TestModelAllTypesReply>> GetListTestModelAllTypesType1000(GrpcTestRequest request, CallContext context = default)
        {
            return await _testData.ListTestModelAllTypesType1000();
        }
    }
}
