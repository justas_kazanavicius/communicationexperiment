﻿using Data;

namespace GrpcData;

public class GrpcClientData : IGrpcClientData
{
    private readonly TestModelOnlyTextTypeReply _testModelOnlyTextType10;
    private readonly TestModelOnlyTextTypeReply _testModelOnlyTextType1000;
    private readonly TestModelOnlyTextTypeReply _testModelOnlyTextType100000;
    private readonly TestModelOnlyTextTypeReply _testModelOnlyTextType1000000;
    private readonly TestModelAllTypesReply _testModelAllTypesType;
    private readonly List<TestModelAllTypesReply> _listTestModelAllTypesType10;
    private readonly List<TestModelAllTypesReply> _listTestModelAllTypesType100;
    private readonly List<TestModelAllTypesReply> _listTestModelAllTypesType1000;

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType10() => await Task.FromResult(_testModelOnlyTextType10);

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType1000() => await Task.FromResult(_testModelOnlyTextType1000);

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType100000() => await Task.FromResult(_testModelOnlyTextType100000);

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType1000000() => await Task.FromResult(_testModelOnlyTextType1000000);

    public async Task<TestModelAllTypesReply> TestModelAllTypesType() => await Task.FromResult(_testModelAllTypesType);

    public async Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType10() => await Task.FromResult(_listTestModelAllTypesType10);

    public async Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType100() => await Task.FromResult(_listTestModelAllTypesType100);

    public async Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType1000() => await Task.FromResult(_listTestModelAllTypesType1000);

    public GrpcClientData(ITestData testData)
    {
        _testModelOnlyTextType10 = new TestModelOnlyTextTypeReply { TextField = testData.TestModelOnlyTextType10().Result.TextField };
        _testModelOnlyTextType1000 = new TestModelOnlyTextTypeReply { TextField = testData.TestModelOnlyTextType1000().Result.TextField };
        _testModelOnlyTextType100000 = new TestModelOnlyTextTypeReply { TextField = testData.TestModelOnlyTextType100000().Result.TextField };
        _testModelOnlyTextType1000000 = new TestModelOnlyTextTypeReply { TextField = testData.TestModelOnlyTextType1000000().Result.TextField };

        _testModelAllTypesType = Map(testData.TestModelAllTypesType().Result);

        _listTestModelAllTypesType10 = testData.ListTestModelAllTypesType10().Result.Select(Map).ToList();
        _listTestModelAllTypesType100 = testData.ListTestModelAllTypesType100().Result.Select(Map).ToList();
        _listTestModelAllTypesType1000 = testData.ListTestModelAllTypesType1000().Result.Select(Map).ToList();
    }

    private TestModelAllTypesReply Map(TestModelAllTypes model)
    {
        return new TestModelAllTypesReply
        {
            TextField = model.TextField,
            BoolField = model.BoolField,
            ByteField = model.ByteField,
            DateTimeField = model.DateTimeField,
            DecimalField = model.DecimalField,
            DoubleField = model.DoubleField,
            FloatField = model.FloatField,
            IntField = model.IntField,
            LongField = model.LongField,
            ShortField = model.ShortField,
        };
    }
}
