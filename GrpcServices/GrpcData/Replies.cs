﻿using System.Runtime.Serialization;

namespace GrpcData;

[DataContract]
public class TestModelOnlyTextTypeReply
{
    [DataMember(Order = 1)]
    public string TextField { get; set; }
}

[DataContract]
public class TestModelAllTypesReply
{
    [DataMember(Order = 1)]
    public string TextField { get; set; }

    [DataMember(Order = 2)]
    public bool BoolField { get; set; }

    [DataMember(Order = 3)]
    public byte ByteField { get; set; }

    [DataMember(Order = 4)]
    public DateTime DateTimeField { get; set; }

    [DataMember(Order = 5)]
    public decimal DecimalField { get; set; }

    [DataMember(Order = 6)]
    public double DoubleField { get; set; }

    [DataMember(Order = 7)]
    public float FloatField { get; set; }

    [DataMember(Order = 8)]
    public int IntField { get; set; }

    [DataMember(Order = 9)]
    public short ShortField { get; set; }

    [DataMember(Order = 10)]
    public long LongField { get; set; }
}
