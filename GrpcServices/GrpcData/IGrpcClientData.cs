﻿namespace GrpcData;

public interface IGrpcClientData
{
    Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType10();

    Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType1000();

    Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType100000();

    Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType1000000();

    Task<TestModelAllTypesReply> TestModelAllTypesType();

    Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType10();

    Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType100();

    Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType1000();
}
