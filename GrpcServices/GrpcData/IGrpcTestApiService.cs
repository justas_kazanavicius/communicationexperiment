﻿using ProtoBuf.Grpc;
using System.ServiceModel;

namespace GrpcData;

[ServiceContract]
public interface IGrpcTestApiService
{
    [OperationContract]
    Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType10(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType1000(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType100000(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<TestModelOnlyTextTypeReply> GetTestModelOnlyTextType1000000(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<TestModelAllTypesReply> GetTestModelAllTypesType(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<List<TestModelAllTypesReply>> GetListTestModelAllTypesType10(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<List<TestModelAllTypesReply>> GetListTestModelAllTypesType100(GrpcTestRequest request, CallContext context = default);

    [OperationContract]
    Task<List<TestModelAllTypesReply>> GetListTestModelAllTypesType1000(GrpcTestRequest request, CallContext context = default);
}
