﻿using GrpcData;

namespace GrpcClient;

public class ClientData : IDisposable, IGrpcClientData
{
    private readonly Client _client;

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType10() => await _client.GetTextOnly10();

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType1000() => await _client.GetTextOnly1000();

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType100000() => await _client.GetTextOnly100000();

    public async Task<TestModelOnlyTextTypeReply> TestModelOnlyTextType1000000() => await _client.GetTextOnly1000000();

    public async Task<TestModelAllTypesReply> TestModelAllTypesType() => await _client.GetAllTypes();

    public async Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType10() => await _client.GetAllTypesList10();

    public async Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType100() => await _client.GetAllTypesList100();

    public async Task<List<TestModelAllTypesReply>> ListTestModelAllTypesType1000() => await _client.GetAllTypesList1000();

    public ClientData(int port)
    {
        _client = new Client(port);
    }

    public void Dispose()
    {
        _client.Dispose();
    }
}
