﻿using Grpc.Net.Client;
using GrpcData;
using ProtoBuf.Grpc.Client;

namespace GrpcClient
{
    public class Client : IDisposable
    {
        private readonly GrpcChannel _channel;

        public Client(int port)
        {
            _channel = GrpcChannel.ForAddress($"http://localhost:{port}");
        }

        public async Task<TestModelOnlyTextTypeReply> GetTextOnly10()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetTestModelOnlyTextType10(new GrpcTestRequest());
        }

        public async Task<TestModelOnlyTextTypeReply> GetTextOnly1000()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetTestModelOnlyTextType1000(new GrpcTestRequest());
        }

        public async Task<TestModelOnlyTextTypeReply> GetTextOnly100000()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetTestModelOnlyTextType100000(new GrpcTestRequest());
        }

        public async Task<TestModelOnlyTextTypeReply> GetTextOnly1000000()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetTestModelOnlyTextType1000000(new GrpcTestRequest());
        }

        public async Task<TestModelAllTypesReply> GetAllTypes()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetTestModelAllTypesType(new GrpcTestRequest());
        }

        public async Task<List<TestModelAllTypesReply>> GetAllTypesList10()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetListTestModelAllTypesType10(new GrpcTestRequest());
        }

        public async Task<List<TestModelAllTypesReply>> GetAllTypesList100()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetListTestModelAllTypesType100(new GrpcTestRequest());
        }

        public async Task<List<TestModelAllTypesReply>> GetAllTypesList1000()
        {
            return await _channel.CreateGrpcService<IGrpcTestApiService>().GetListTestModelAllTypesType1000(new GrpcTestRequest());
        }

        public void Dispose()
        {
            _channel.Dispose();
        }
    }
}
