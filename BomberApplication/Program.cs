﻿using NBomber.Contracts;
using NBomber.CSharp;
using RabbitMQClient;

namespace BomberApplication
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Start load test");
            Console.ReadLine();

            //================================================================================================================
            //using var kafkaClient = new KafkaClient.KafkaClient(1);

            //var kafkaStep = Step.Create("kafka", async context =>
            //{
            //    var response = await kafkaClient.GetAllTypesList1000();
            //    return response != null ? Response.Ok() : Response.Fail();
            //});

            //var kafkaScenario = ScenarioBuilder
            //    .CreateScenario("kafka", kafkaStep)
            //    .WithWarmUpDuration(TimeSpan.FromSeconds(5))
            //    .WithLoadSimulations(LoadSimulations());

            //================================================================================================================
            //using var rabbitClient = new Client(1);

            //var rabbitStep = Step.Create("rabbit", async context =>
            //{
            //    var response = await rabbitClient.GetAllTypesList100();
            //    return response != null ? Response.Ok() : Response.Fail();
            //});

            //var rabbitScenario = ScenarioBuilder
            //    .CreateScenario("rabbit", rabbitStep)
            //    .WithWarmUpDuration(TimeSpan.FromSeconds(5))
            //    .WithLoadSimulations(LoadSimulations());

            //================================================================================================================
            using var httpClient = new HttpClient.Client(5301);

            var httpStep = Step.Create("http", async context =>
            {
                var response = await httpClient.GetTextOnly1000();
                return response != null ? Response.Ok() : Response.Fail();
            });

            var httpScenario = ScenarioBuilder
                .CreateScenario("http", httpStep)
                .WithWarmUpDuration(TimeSpan.FromSeconds(5))
                .WithLoadSimulations(LoadSimulations());

            //================================================================================================================
            //using var grpcClient = new GrpcClient.Client(5401);

            //var grpcStep = Step.Create("grpc", async context =>
            //{
            //    var response = await grpcClient.GetAllTypesList1000();
            //    return response != null ? Response.Ok() : Response.Fail();
            //});

            //var grpcScenario = ScenarioBuilder
            //    .CreateScenario("grpc", grpcStep)
            //    .WithWarmUpDuration(TimeSpan.FromSeconds(5))
            //    .WithLoadSimulations(LoadSimulations());

            //================================================================================================================
            //using var graphQlClient = new GraphQLClient.Client(5201);

            //var graphQlStep = Step.Create("graphQl", async context =>
            //{
            //    var response = await graphQlClient.GetAllTypesList1000();
            //    return response != null ? Response.Ok() : Response.Fail();
            //});

            //var graphQlScenario = ScenarioBuilder
            //    .CreateScenario("graphQl", graphQlStep)
            //    .WithWarmUpDuration(TimeSpan.FromSeconds(5))
            //    .WithLoadSimulations(LoadSimulations());

            NBomberRunner
                .RegisterScenarios(new[]
                {
                    //kafkaScenario,
                    //rabbitScenario,
                    httpScenario,
                    //grpcScenario,
                    //graphQlScenario
                })
                .Run();

            Console.WriteLine("Finished step");
            Console.ReadLine();
        }

        private static LoadSimulation[] LoadSimulations()
        {
            return new[]
            {
                Simulation.KeepConstant(10, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(20, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(30, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(40, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(50, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(60, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(70, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(80, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(90, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(100, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(110, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(120, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(130, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(140, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(150, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(160, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(170, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(180, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(190, TimeSpan.FromSeconds(30)),
                Simulation.KeepConstant(200, TimeSpan.FromSeconds(30)),

                //Simulation.InjectPerSec(rate: 10, during: TimeSpan.FromSeconds(30)),
                //Simulation.InjectPerSec(rate: 50, during: TimeSpan.FromSeconds(30)),
                //Simulation.InjectPerSec(rate: 100, during: TimeSpan.FromSeconds(30)),
                //Simulation.InjectPerSec(rate: 120, during: TimeSpan.FromSeconds(30)),
                //Simulation.InjectPerSec(rate: 500, during: TimeSpan.FromSeconds(30)),
                //Simulation.InjectPerSec(rate: 1000, during: TimeSpan.FromSeconds(30)),
            };
        }
    }
}
