using RabbitMQClient;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton(new Client(1));
builder.Services.AddSingleton(new KafkaClient.KafkaClient(1));

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();
