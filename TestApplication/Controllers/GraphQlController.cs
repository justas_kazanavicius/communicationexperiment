using Data;
using GraphQLClient;
using Microsoft.AspNetCore.Mvc;

namespace TestApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class GraphQlController : ControllerBase
    {
        private readonly Client _client;

        public GraphQlController()
        {
            _client = new Client(5201);
        }

        [HttpGet]
        [ActionName("GetTextOnly10")]
        public async Task<TestModelOnlyText> GetTextOnly10() => await _client.GetTextOnly10();

        [HttpGet]
        [ActionName("GetTextOnly1000")]
        public async Task<TestModelOnlyText> GetTextOnly1000() => await _client.GetTextOnly1000();

        [HttpGet]
        [ActionName("GetTextOnly100000")]
        public async Task<TestModelOnlyText> GetTextOnly100000() => await _client.GetTextOnly100000();

        [HttpGet]
        [ActionName("GetTextOnly1000000")]
        public async Task<TestModelOnlyText> GetTextOnly1000000() => await _client.GetTextOnly1000000();

        [HttpGet]
        [ActionName("GetAllTypes")]
        public async Task<TestModelAllTypes> GetAllTypes() => await _client.GetAllTypes();

        [HttpGet]
        [ActionName("GetAllTypesList10")]
        public async Task<List<TestModelAllTypes>> GetAllTypesList10() => await _client.GetAllTypesList10();

        [HttpGet]
        [ActionName("GetAllTypesList100")]
        public async Task<List<TestModelAllTypes>> GetAllTypesList100() => await _client.GetAllTypesList100();

        [HttpGet]
        [ActionName("GetAllTypesList1000")]
        public async Task<List<TestModelAllTypes>> GetAllTypesList1000() => await _client.GetAllTypesList1000();
    }
}
