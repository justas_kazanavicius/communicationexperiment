using GrpcClient;
using GrpcData;
using Microsoft.AspNetCore.Mvc;

namespace TestApplication.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class GrpcController : ControllerBase
{
    private readonly Client _client;

    public GrpcController()
    {
        _client = new Client(5401);
    }

    [HttpGet]
    [ActionName("GetTextOnly10")]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly10()
    {
        return await _client.GetTextOnly10();
    }

    [HttpGet]
    [ActionName("GetTextOnly1000")]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly1000()
    {
        return await _client.GetTextOnly1000();
    }

    [HttpGet]
    [ActionName("GetTextOnly100000")]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly100000()
    {
        return await _client.GetTextOnly100000();
    }

    [HttpGet]
    [ActionName("GetTextOnly1000000")]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly1000000()
    {
        return await _client.GetTextOnly1000000();
    }

    [HttpGet]
    [ActionName("GetAllTypes")]
    public async Task<TestModelAllTypesReply> GetAllTypes()
    {
        return await _client.GetAllTypes();
    }

    [HttpGet]
    [ActionName("GetAllTypesList10")]
    public async Task<List<TestModelAllTypesReply>> GetAllTypesList10()
    {
        return await _client.GetAllTypesList10();
    }

    [HttpGet]
    [ActionName("GetAllTypesList100")]
    public async Task<List<TestModelAllTypesReply>> GetAllTypesList100()
    {
        return await _client.GetAllTypesList100();
    }

    [HttpGet]
    [ActionName("GetAllTypesList1000")]
    public async Task<List<TestModelAllTypesReply>> GetAllTypesList1000()
    {
        return await _client.GetAllTypesList1000();
    }
}
