using Data;
using HttpClient;
using Microsoft.AspNetCore.Mvc;

namespace TestApplication.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class HttpController : ControllerBase
{
    private readonly Client _client;

    public HttpController()
    {
        _client = new Client(5301);
    }

    [HttpGet]
    [ActionName("GetTextOnly10")]
    public async Task<TestModelOnlyText> GetTextOnly10()
    {
        return await _client.GetTextOnly10();
    }

    [HttpGet]
    [ActionName("GetTextOnly1000")]
    public async Task<TestModelOnlyText> GetTextOnly1000()
    {
        return await _client.GetTextOnly1000();
    }

    [HttpGet]
    [ActionName("GetTextOnly100000")]
    public async Task<TestModelOnlyText> GetTextOnly100000()
    {
        return await _client.GetTextOnly100000();
    }

    [HttpGet]
    [ActionName("GetTextOnly1000000")]
    public async Task<TestModelOnlyText> GetTextOnly1000000()
    {
        return await _client.GetTextOnly1000000();
    }

    [HttpGet]
    [ActionName("GetAllTypes")]
    public async Task<TestModelAllTypes> GetAllTypes()
    {
        return await _client.GetAllTypes();
    }

    [HttpGet]
    [ActionName("GetAllTypesList10")]
    public async Task<List<TestModelAllTypes>> GetAllTypesList10()
    {
        return await _client.GetAllTypesList10();
    }

    [HttpGet]
    [ActionName("GetAllTypesList100")]
    public async Task<List<TestModelAllTypes>> GetAllTypesList100()
    {
        return await _client.GetAllTypesList100();
    }

    [HttpGet]
    [ActionName("GetAllTypesList1000")]
    public async Task<List<TestModelAllTypes>> GetAllTypesList1000()
    {
        return await _client.GetAllTypesList1000();
    }
}
