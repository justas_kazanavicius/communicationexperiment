﻿using Confluent.Kafka;
using Simple.Kafka.Rpc;
using System.Text;
using System.Text.Json;

namespace KafkaHelp
{
    public class KafkaRpcSever<TReq, TRes> : IDisposable
    {
        private readonly string _responseTopic;
        private readonly CancellationTokenSource _stop = new CancellationTokenSource();

        public KafkaRpcSever(string requestTopic, Func<TReq, Task<TRes>> handlerAsync)
        {
            _responseTopic = $"{requestTopic}Response";

            var consumer = new ConsumerBuilder<byte[], byte[]>(new ConsumerConfig
            {
                EnableAutoCommit = true,
                BootstrapServers = "127.0.0.1:9092",
                AutoOffsetReset = AutoOffsetReset.Earliest,
                GroupId = "Simple.Kafka.Rpc.Todo.Server.Group",
                BrokerAddressFamily = BrokerAddressFamily.V4,
                AllowAutoCreateTopics = true,
                FetchMinBytes = 1
            }).Build();

            var producer = new ProducerBuilder<byte[], byte[]>(new ProducerConfig
            {
                BootstrapServers = "127.0.0.1:9092",
                BrokerAddressFamily = BrokerAddressFamily.V4,
                MessageMaxBytes = 2048000,

                //low latency settings
                LingerMs = 0,
                CompressionType = CompressionType.None,
                Acks = Acks.Leader,
                EnableIdempotence = false,
                BatchNumMessages = 1,
            }).Build();
            consumer.Subscribe(new[] { requestTopic });

            // Consumer thread to consume messages from topics
            var consumerThread = Task.Factory.StartNew(async () => { await HandleRequest(consumer, producer, handlerAsync); }, _stop.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        private async Task HandleRequest(IConsumer<byte[], byte[]>? consumer, IProducer<byte[], byte[]>? producer, Func<TReq, Task<TRes>> handlerAsync)
        {
            try
            {
                var token = _stop.Token;
                while (!token.IsCancellationRequested)
                {
                    var result = consumer.Consume(token);
                    if (result == null || result.IsPartitionEOF) continue;

                    var rpcRequestId = result.Message.GetRpcRequestId();
                    if (rpcRequestId == null) continue;

                    var rpcRequestIdParseResult = rpcRequestId.ParseRpcRequestId();
                    if (!rpcRequestIdParseResult.IsOk) continue;

                    var message = Encoding.UTF8.GetString(result.Message.Value);
                    var deserialize = JsonSerializer.Deserialize<TReq>(message);
                    var response = await handlerAsync(deserialize);
                    var serialize = JsonSerializer.Serialize(response);
                    var responseBytes = Encoding.UTF8.GetBytes(serialize);

                    await producer.ProduceAsync(_responseTopic, new Message<byte[], byte[]>
                    {
                        Key = Array.Empty<byte>(),
                        Value = responseBytes
                    }.WithRpcRequestId(rpcRequestId));
                }
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unhandled exception occurred: {ex}");
            }
        }

        public void Dispose()
        {
            _stop.Cancel();
        }
    }
}
