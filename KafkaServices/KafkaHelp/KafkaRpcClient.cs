﻿using Confluent.Kafka;
using Simple.Kafka.Rpc;
using System.Text;
using System.Text.Json;

namespace KafkaHelp
{
    public class KafkaRpcClient<TReq, TRes> : IDisposable
    {
        private readonly string _requestTopic;

        private readonly RpcClient _rpc;

        public KafkaRpcClient(string requestTopic)
        {
            _requestTopic = requestTopic;

            _rpc = RpcClient.Create(b =>
            {
                b.Config.Topics = new[] { $"{requestTopic}Response" };

                b.Consumer.Kafka.BootstrapServers = "127.0.0.1:9092";
                b.Consumer.Kafka.FetchMinBytes = 1;

                b.Producer.Kafka.BootstrapServers = "127.0.0.1:9092";

                //low latency settings
                b.Producer.Kafka.LingerMs = 0;
                b.Producer.Kafka.BatchNumMessages = 1;
                b.Producer.Kafka.Acks = Acks.Leader;
                b.Producer.Kafka.EnableIdempotence = false;
                b.Producer.Kafka.CompressionType = CompressionType.None;
            });
        }

        public async Task<TRes> Call(TReq message)
        {
            var serialize = JsonSerializer.Serialize(message);
            var messageBytes = Encoding.UTF8.GetBytes(serialize);

            var result =
                await _rpc.SendAsync(new Message<byte[], byte[]> { Key = Array.Empty<byte>(), Value = messageBytes },
                    _requestTopic);

            var response = Encoding.UTF8.GetString(result.Message.Value);
            return JsonSerializer.Deserialize<TRes>(response);
        }

        public void Dispose()
        {
            _rpc.Dispose();
        }
    }
}
