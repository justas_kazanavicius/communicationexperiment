﻿using Data;
using KafkaClient;
using KafkaHelp;
using Microsoft.Extensions.Hosting;

namespace KafkaService2
{
    public class Worker : IHostedService, IDisposable
    {
        private readonly int _number = 2;
        private readonly List<IDisposable> _disposables = new List<IDisposable>();
        private readonly KafkaClient.KafkaClient _client = new KafkaClient.KafkaClient(3);

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, TestModelOnlyText>($"TestModelOnlyTextType10-{_number}", TestModelOnlyTextType10)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, TestModelOnlyText>($"TestModelOnlyTextType1000-{_number}", TestModelOnlyTextType1000)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, TestModelOnlyText>($"TestModelOnlyTextType100000-{_number}", TestModelOnlyTextType100000)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, TestModelOnlyText>($"TestModelOnlyTextType1000000-{_number}", TestModelOnlyTextType1000000)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, TestModelAllTypes>($"TestModelAllTypesType-{_number}", TestModelAllTypesType)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType10-{_number}", ListTestModelAllTypesType10)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType100-{_number}", ListTestModelAllTypesType100)); });
            Task.Run(() => { _disposables.Add(new KafkaRpcSever<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType1000-{_number}", ListTestModelAllTypesType1000)); });

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            DisposeAll();
            return Task.CompletedTask;
        }

        private async Task<TestModelOnlyText> TestModelOnlyTextType10(Request request)
        {
            return await _client.GetTextOnly10();
        }

        private async Task<TestModelOnlyText> TestModelOnlyTextType1000(Request request)
        {
            return await _client.GetTextOnly1000();
        }

        private async Task<TestModelOnlyText> TestModelOnlyTextType100000(Request request)
        {
            return await _client.GetTextOnly100000();
        }

        private async Task<TestModelOnlyText> TestModelOnlyTextType1000000(Request request)
        {
            return await _client.GetTextOnly1000000();
        }

        private async Task<TestModelAllTypes> TestModelAllTypesType(Request request)
        {
            return await _client.GetAllTypes();
        }

        private async Task<List<TestModelAllTypes>> ListTestModelAllTypesType10(Request request)
        {
            return await _client.GetAllTypesList10();
        }

        private async Task<List<TestModelAllTypes>> ListTestModelAllTypesType100(Request request)
        {
            return await _client.GetAllTypesList100();
        }

        private async Task<List<TestModelAllTypes>> ListTestModelAllTypesType1000(Request request)
        {
            return await _client.GetAllTypesList1000();
        }

        private void DisposeAll()
        {
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
        }

        public void Dispose()
        {
            DisposeAll();
        }
    }
}
