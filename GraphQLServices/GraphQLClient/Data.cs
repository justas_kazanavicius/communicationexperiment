﻿using Data;

namespace GraphQLClient;

public class Data
{
    public TestModelOnlyText textOnly10 { get; set; }
    public TestModelOnlyText textOnly1000 { get; set; }
    public TestModelOnlyText textOnly100000 { get; set; }
    public TestModelOnlyText textOnly1000000 { get; set; }
    public TestModelAllTypes allTypes { get; set; }
    public List<TestModelAllTypes> allTypesList10 { get; set; }
    public List<TestModelAllTypes> allTypesList100 { get; set; }
    public List<TestModelAllTypes> allTypesList1000 { get; set; }
}
