﻿using Data;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.SystemTextJson;

namespace GraphQLClient
{
    public class Client : IDisposable
    {
        private readonly GraphQLHttpClient _client;

        public Client(int port)
        {
            _client = new GraphQLHttpClient($@"http://localhost:{port}/graphql", new SystemTextJsonSerializer());
        }

        public async Task<TestModelOnlyText> GetTextOnly10()
        {
            var requestText = @"
            {
              textOnly10 {
                textField
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.textOnly10;
        }

        public async Task<TestModelOnlyText> GetTextOnly1000()
        {
            var requestText = @"
            {
              textOnly1000 {
                textField
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.textOnly1000;
        }

        public async Task<TestModelOnlyText> GetTextOnly100000()
        {
            var requestText = @"
            {
              textOnly100000 {
                textField
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.textOnly100000;
        }

        public async Task<TestModelOnlyText> GetTextOnly1000000()
        {
            var requestText = @"
            {
              textOnly1000000 {
                textField
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.textOnly1000000;
        }

        public async Task<TestModelAllTypes> GetAllTypes()
        {
            var requestText = @"
            {
              allTypes {
                textField,
                boolField,
                byteField,
                dateTimeField,
                decimalField,
                doubleField,
                floatField,
                intField,
                longField,
                shortField,
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.allTypes;
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList10()
        {
            var requestText = @"
            {
              allTypesList10 {
                textField,
                boolField,
                byteField,
                dateTimeField,
                decimalField,
                doubleField,
                floatField,
                intField,
                longField,
                shortField,
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.allTypesList10;
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList100()
        {
            var requestText = @"
            {
              allTypesList100 {
                textField,
                boolField,
                byteField,
                dateTimeField,
                decimalField,
                doubleField,
                floatField,
                intField,
                longField,
                shortField,
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.allTypesList100;
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList1000()
        {
            var requestText = @"
            {
              allTypesList1000 {
                textField,
                boolField,
                byteField,
                dateTimeField,
                decimalField,
                doubleField,
                floatField,
                intField,
                longField,
                shortField,
              }
            }";

            var request = new GraphQLRequest { Query = requestText };
            var response = await _client.SendQueryAsync<Data>(request);

            return response.Data.allTypesList1000;
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}
