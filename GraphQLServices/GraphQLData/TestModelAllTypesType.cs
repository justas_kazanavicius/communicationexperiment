﻿using Data;
using GraphQL.Types;

namespace GraphQLData;

public class TestModelAllTypesType : ObjectGraphType<TestModelAllTypes>
{
    public TestModelAllTypesType()
    {
        Field(d => d.TextField, nullable: true);
        Field(d => d.BoolField, nullable: false);
        Field(d => d.ByteField, nullable: false);
        Field(d => d.DateTimeField, nullable: false);
        Field(d => d.DecimalField, nullable: false);
        Field(d => d.DoubleField, nullable: false);
        Field(d => d.FloatField, nullable: false);
        Field(d => d.IntField, nullable: false);
        Field(d => d.ShortField, nullable: false);
        Field(d => d.LongField, nullable: false);
    }
}
