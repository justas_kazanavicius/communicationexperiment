﻿using Data;
using GraphQL.Types;

namespace GraphQLData;

public class TestQueries : ObjectGraphType
{
    public TestQueries(ITestData testData)
    {
        FieldAsync<TestModelOnlyTextType>("textOnly10", resolve: async _ => await testData.TestModelOnlyTextType10());
        FieldAsync<TestModelOnlyTextType>("textOnly1000", resolve: async _ => await testData.TestModelOnlyTextType1000());
        FieldAsync<TestModelOnlyTextType>("textOnly100000", resolve: async _ => await testData.TestModelOnlyTextType100000());
        FieldAsync<TestModelOnlyTextType>("textOnly1000000", resolve: async _ => await testData.TestModelOnlyTextType1000000());
        FieldAsync<TestModelAllTypesType>("allTypes", resolve: async _ => await testData.TestModelAllTypesType());
        FieldAsync<ListGraphType<TestModelAllTypesType>>("allTypesList10", resolve: async _ => await testData.ListTestModelAllTypesType10());
        FieldAsync<ListGraphType<TestModelAllTypesType>>("allTypesList100", resolve: async _ => await testData.ListTestModelAllTypesType100());
        FieldAsync<ListGraphType<TestModelAllTypesType>>("allTypesList1000", resolve: async _ => await testData.ListTestModelAllTypesType1000());
    }
}
