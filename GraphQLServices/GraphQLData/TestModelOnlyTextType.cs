﻿using Data;
using GraphQL.Types;

namespace GraphQLData;

public class TestModelOnlyTextType : ObjectGraphType<TestModelOnlyText>
{
    public TestModelOnlyTextType()
    {
        Field(d => d.TextField, nullable: true);
    }
}
