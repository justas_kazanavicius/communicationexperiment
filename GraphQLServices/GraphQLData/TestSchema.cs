﻿using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace GraphQLData
{
    public class TestSchema : Schema
    {
        public TestSchema(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<TestQueries>();
        }
    }
}
