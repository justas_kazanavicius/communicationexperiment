using Data;
using GraphQL.MicrosoftDI;
using GraphQL.Server;
using GraphQL.Types;
using GraphQLData;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<ITestData>(new ClientData(5204));
builder.Services.AddSingleton<ISchema, TestSchema>(services => new TestSchema(new SelfActivatingServiceProvider(services)));
builder.Services.AddGraphQL(options => { options.EnableMetrics = false; }).AddSystemTextJson();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseGraphQLAltair();
}

app.UseHttpsRedirection();
app.UseGraphQL<ISchema>();
app.Run();
