﻿using Data;
using Microsoft.Extensions.Hosting;
using RabbitMQData;
using RabbitMQHelp;

namespace RabbitMQService5;

public class Worker : IHostedService, IDisposable
{
    private readonly int _number = 5;
    private readonly List<IDisposable> _disposables = new List<IDisposable>();
    private readonly ITestData _testData = new TestData();

    public Task StartAsync(CancellationToken cancellationToken)
    {
        Task.Run(() => { _disposables.Add(new RpcServer<Request, TestModelOnlyText>($"TestModelOnlyTextType10-{_number}", TestModelOnlyTextType10)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, TestModelOnlyText>($"TestModelOnlyTextType1000-{_number}", TestModelOnlyTextType1000)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, TestModelOnlyText>($"TestModelOnlyTextType100000-{_number}", TestModelOnlyTextType100000)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, TestModelOnlyText>($"TestModelOnlyTextType1000000-{_number}", TestModelOnlyTextType1000000)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, TestModelAllTypes>($"TestModelAllTypesType-{_number}", TestModelAllTypesType)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType10-{_number}", ListTestModelAllTypesType10)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType100-{_number}", ListTestModelAllTypesType100)); });
        Task.Run(() => { _disposables.Add(new RpcServer<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType1000-{_number}", ListTestModelAllTypesType1000)); });

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        DisposeAll();
        return Task.CompletedTask;
    }

    private TestModelOnlyText TestModelOnlyTextType10(Request request)
    {
        return _testData.TestModelOnlyTextType10().Result;
    }

    private TestModelOnlyText TestModelOnlyTextType1000(Request request)
    {
        return _testData.TestModelOnlyTextType1000().Result;
    }

    private TestModelOnlyText TestModelOnlyTextType100000(Request request)
    {
        return _testData.TestModelOnlyTextType100000().Result;
    }

    private TestModelOnlyText TestModelOnlyTextType1000000(Request request)
    {
        return _testData.TestModelOnlyTextType1000000().Result;
    }

    private TestModelAllTypes TestModelAllTypesType(Request request)
    {
        return _testData.TestModelAllTypesType().Result;
    }

    private List<TestModelAllTypes> ListTestModelAllTypesType10(Request request)
    {
        return _testData.ListTestModelAllTypesType10().Result;
    }

    private List<TestModelAllTypes> ListTestModelAllTypesType100(Request request)
    {
        return _testData.ListTestModelAllTypesType100().Result;
    }

    private List<TestModelAllTypes> ListTestModelAllTypesType1000(Request request)
    {
        return _testData.ListTestModelAllTypesType1000().Result;
    }

    private void DisposeAll()
    {
        foreach (var disposable in _disposables)
        {
            disposable.Dispose();
        }
    }

    public void Dispose()
    {
        DisposeAll();
    }
}
