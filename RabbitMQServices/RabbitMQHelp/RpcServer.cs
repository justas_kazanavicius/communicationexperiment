﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace RabbitMQHelp
{
    public class RpcServer<TReq, TRes> : IDisposable
    {
        private readonly Func<TReq, TRes> _handler;
        private readonly Func<TReq, Task<TRes>> _handlerAsync;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public RpcServer(string queueName, Func<TReq, TRes> handler)
        {
            _handler = handler;
            var factory = new ConnectionFactory { HostName = "localhost" };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
            _channel.BasicQos(0, 1, false);
            var consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);

            consumer.Received += OnReceived();
        }

        public RpcServer(string queueName, Func<TReq, Task<TRes>> handlerAsync)
        {
            _handlerAsync = handlerAsync;
            var factory = new ConnectionFactory { HostName = "localhost" };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
            _channel.BasicQos(0, 1, false);
            var consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);

            consumer.Received += OnReceivedAsync();
        }

        private EventHandler<BasicDeliverEventArgs> OnReceived()
        {
            return (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var props = ea.BasicProperties;
                var replyProps = _channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;

                var message = Encoding.UTF8.GetString(body);
                var deserialize = JsonSerializer.Deserialize<TReq>(message);
                var response = _handler(deserialize);
                var serialize = JsonSerializer.Serialize(response);
                var responseBytes = Encoding.UTF8.GetBytes(serialize);

                _channel.BasicPublish(exchange: "", routingKey: props.ReplyTo, basicProperties: replyProps, body: responseBytes);
                _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
        }

        private EventHandler<BasicDeliverEventArgs> OnReceivedAsync()
        {
            return (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var props = ea.BasicProperties;
                var replyProps = _channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;

                var message = Encoding.UTF8.GetString(body);
                var deserialize = JsonSerializer.Deserialize<TReq>(message);
                var response = _handlerAsync(deserialize).Result;
                var serialize = JsonSerializer.Serialize(response);
                var responseBytes = Encoding.UTF8.GetBytes(serialize);

                _channel.BasicPublish(exchange: "", routingKey: props.ReplyTo, basicProperties: replyProps, body: responseBytes);
                _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}
