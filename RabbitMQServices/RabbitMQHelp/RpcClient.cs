﻿using Nito.AsyncEx;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace RabbitMQHelp;

public class RpcClient<TReq, TRes> : IDisposable
{
    private readonly string _queueName;
    private readonly IConnection _connection;
    private readonly IModel _channel;
    private readonly string _replyQueueName;
    private readonly EventingBasicConsumer _consumer;

    //private readonly BlockingCollection<TRes> _respQueue = new BlockingCollection<TRes>();
    private new readonly AsyncProducerConsumerQueue<TRes> _respQueue = new AsyncProducerConsumerQueue<TRes>();

    private readonly IBasicProperties _props;

    public RpcClient(string queueName)
    {
        _queueName = queueName;
        var factory = new ConnectionFactory { HostName = "localhost" };

        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _replyQueueName = _channel.QueueDeclare().QueueName;
        _consumer = new EventingBasicConsumer(_channel);

        _props = _channel.CreateBasicProperties();
        var correlationId = Guid.NewGuid().ToString();
        _props.CorrelationId = correlationId;
        _props.ReplyTo = _replyQueueName;

        _consumer.Received += (model, ea) =>
        {
            var body = ea.Body.ToArray();
            var response = Encoding.UTF8.GetString(body);
            var deserialize = JsonSerializer.Deserialize<TRes>(response);
            if (ea.BasicProperties.CorrelationId == correlationId)
            {
                //_respQueue.Add(deserialize);
                _respQueue.EnqueueAsync(deserialize).Wait(2000);
            }
        };

        _channel.BasicConsume(consumer: _consumer, queue: _replyQueueName, autoAck: true);
    }

    public async Task<TRes> Call(TReq message)
    {
        var deserialize = JsonSerializer.Serialize(message);
        var messageBytes = Encoding.UTF8.GetBytes(deserialize);

        _channel.BasicPublish(exchange: "", routingKey: _queueName, basicProperties: _props, body: messageBytes);

        return await TaskWithTimeoutAndException(_respQueue.DequeueAsync(), TimeSpan.FromSeconds(10));
    }

    private static async Task<T> TaskWithTimeoutAndException<T>(Task<T> task, TimeSpan timeout)
    {
        return await await Task.WhenAny(task, DelayedTimeoutExceptionTask<T>(timeout));
    }

    private static async Task<T> DelayedTimeoutExceptionTask<T>(TimeSpan delay)
    {
        await Task.Delay(delay);
        throw new TimeoutException();
    }

    public void Dispose()
    {
        _connection?.Dispose();
        _channel?.Dispose();
    }
}
