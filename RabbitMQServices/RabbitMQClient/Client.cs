﻿using Data;
using RabbitMQData;
using RabbitMQHelp;

namespace RabbitMQClient
{
    public class Client : IDisposable
    {
        private readonly RpcClient<Request, TestModelOnlyText> _testModelOnlyTextType10;
        private readonly RpcClient<Request, TestModelOnlyText> _testModelOnlyTextType1000;
        private readonly RpcClient<Request, TestModelOnlyText> _testModelOnlyTextType100000;
        private readonly RpcClient<Request, TestModelOnlyText> _testModelOnlyTextType1000000;
        private readonly RpcClient<Request, TestModelAllTypes> _testModelAllTypesType;
        private readonly RpcClient<Request, List<TestModelAllTypes>> _listTestModelAllTypesType10;
        private readonly RpcClient<Request, List<TestModelAllTypes>> _listTestModelAllTypesType100;
        private readonly RpcClient<Request, List<TestModelAllTypes>> _listTestModelAllTypesType1000;

        public Client(int number)
        {
            _testModelOnlyTextType10 = new RpcClient<Request, TestModelOnlyText>($"TestModelOnlyTextType10-{number}");
            _testModelOnlyTextType1000 = new RpcClient<Request, TestModelOnlyText>($"TestModelOnlyTextType1000-{number}");
            _testModelOnlyTextType100000 = new RpcClient<Request, TestModelOnlyText>($"TestModelOnlyTextType100000-{number}");
            _testModelOnlyTextType1000000 = new RpcClient<Request, TestModelOnlyText>($"TestModelOnlyTextType1000000-{number}");
            _testModelAllTypesType = new RpcClient<Request, TestModelAllTypes>($"TestModelAllTypesType-{number}");
            _listTestModelAllTypesType10 = new RpcClient<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType10-{number}");
            _listTestModelAllTypesType100 = new RpcClient<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType100-{number}");
            _listTestModelAllTypesType1000 = new RpcClient<Request, List<TestModelAllTypes>>($"ListTestModelAllTypesType1000-{number}");
        }

        public async Task<TestModelOnlyText> GetTextOnly10()
        {
            return await _testModelOnlyTextType10.Call(new Request());
        }

        public async Task<TestModelOnlyText> GetTextOnly1000()
        {
            return await _testModelOnlyTextType1000.Call(new Request());
        }

        public async Task<TestModelOnlyText> GetTextOnly100000()
        {
            return await _testModelOnlyTextType100000.Call(new Request());
        }

        public async Task<TestModelOnlyText> GetTextOnly1000000()
        {
            return await _testModelOnlyTextType1000000.Call(new Request());
        }

        public async Task<TestModelAllTypes> GetAllTypes()
        {
            return await _testModelAllTypesType.Call(new Request());
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList10()
        {
            return await _listTestModelAllTypesType10.Call(new Request());
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList100()
        {
            return await _listTestModelAllTypesType100.Call(new Request());
        }

        public async Task<List<TestModelAllTypes>> GetAllTypesList1000()
        {
            return await _listTestModelAllTypesType1000.Call(new Request());
        }

        public void Dispose()
        {
            _testModelOnlyTextType10.Dispose();
            _testModelOnlyTextType1000.Dispose();
            _testModelOnlyTextType100000.Dispose();
            _testModelOnlyTextType1000000.Dispose();
            _testModelAllTypesType.Dispose();
            _listTestModelAllTypesType10.Dispose();
            _listTestModelAllTypesType100.Dispose();
            _listTestModelAllTypesType1000.Dispose();
        }
    }
}
