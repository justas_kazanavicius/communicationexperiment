﻿using BenchmarkDotNet.Attributes;
using GrpcClient;
using GrpcData;

namespace BenchmarkApplication;

[RankColumn, MinColumn, MaxColumn, Q1Column, Q3Column, AllStatisticsColumn]
public class GrpcTestCases
{
    private readonly Client _client;

    public GrpcTestCases()
    {
        _client = new Client(5401);
    }

    [Benchmark]
    public async Task<TestModelOnlyTextTypeReply> GetWarm() => await _client.GetTextOnly10();

    [Benchmark(Baseline = true)]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly10() => await _client.GetTextOnly10();

    [Benchmark]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly1000() => await _client.GetTextOnly1000();

    [Benchmark]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly100000() => await _client.GetTextOnly100000();

    [Benchmark]
    public async Task<TestModelOnlyTextTypeReply> GetTextOnly1000000() => await _client.GetTextOnly1000000();

    [Benchmark]
    public async Task<TestModelAllTypesReply> GetAllTypes() => await _client.GetAllTypes();

    [Benchmark]
    public async Task<List<TestModelAllTypesReply>> GetAllTypesList10() => await _client.GetAllTypesList10();

    [Benchmark]
    public async Task<List<TestModelAllTypesReply>> GetAllTypesList100() => await _client.GetAllTypesList100();

    [Benchmark]
    public async Task<List<TestModelAllTypesReply>> GetAllTypesList1000() => await _client.GetAllTypesList1000();
}
