﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;
using RabbitMQClient;

namespace BenchmarkApplication;

public class Program
{
    public static void Main()
    {
        Console.WriteLine("Press any key to start");
        Console.ReadLine();

        BenchmarkRunner.Run(typeof(Program).Assembly);

        Console.ReadLine();
    }
}
