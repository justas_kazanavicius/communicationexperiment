﻿using BenchmarkDotNet.Attributes;
using Data;
using HttpClient;

namespace BenchmarkApplication;

[RankColumn, MinColumn, MaxColumn, Q1Column, Q3Column, AllStatisticsColumn]
public class HttpTestCases
{
    private readonly Client _client;

    public HttpTestCases()
    {
        _client = new Client(5301);
    }

    [Benchmark]
    public async Task<TestModelOnlyText> GetWarm() => await _client.GetTextOnly10();

    [Benchmark(Baseline = true)]
    public async Task<TestModelOnlyText> GetTextOnly10() => await _client.GetTextOnly10();

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly1000() => await _client.GetTextOnly1000();

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly100000() => await _client.GetTextOnly100000();

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly1000000() => await _client.GetTextOnly1000000();

    [Benchmark]
    public async Task<TestModelAllTypes> GetAllTypes() => await _client.GetAllTypes();

    [Benchmark]
    public async Task<List<TestModelAllTypes>> GetAllTypesList10() => await _client.GetAllTypesList10();

    [Benchmark]
    public async Task<List<TestModelAllTypes>> GetAllTypesList100() => await _client.GetAllTypesList100();

    [Benchmark]
    public async Task<List<TestModelAllTypes>> GetAllTypesList1000() => await _client.GetAllTypesList1000();
}
