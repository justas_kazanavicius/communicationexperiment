﻿using BenchmarkDotNet.Attributes;
using Data;
using RabbitMQClient;

namespace BenchmarkApplication;

[RankColumn, MinColumn, MaxColumn, Q1Column, Q3Column, AllStatisticsColumn]
public class RabbitMqTestCases : IDisposable
{
    private readonly Client _client = new Client(1);

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly10() => await _client.GetTextOnly10();

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly1000() => await _client.GetTextOnly1000();

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly100000() => await _client.GetTextOnly100000();

    [Benchmark]
    public async Task<TestModelOnlyText> GetTextOnly1000000() => await _client.GetTextOnly1000000();

    [Benchmark]
    public async Task<TestModelAllTypes> GetAllTypes() => await _client.GetAllTypes();

    [Benchmark]
    public async Task<List<TestModelAllTypes>> GetAllTypesList10() => await _client.GetAllTypesList10();

    [Benchmark]
    public async Task<List<TestModelAllTypes>> GetAllTypesList100() => await _client.GetAllTypesList100();

    [Benchmark]
    public async Task<List<TestModelAllTypes>> GetAllTypesList1000() => await _client.GetAllTypesList1000();

    public void Dispose()
    {
        _client?.Dispose();
    }
}
