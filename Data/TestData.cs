﻿using System.Text;

namespace Data;

public class TestData : ITestData
{
    private TestModelOnlyText _testModelOnlyTextType10 = GetTestModelOnlyTextType(10);
    private TestModelOnlyText _testModelOnlyTextType1000 = GetTestModelOnlyTextType(1000);
    private TestModelOnlyText _testModelOnlyTextType100000 = GetTestModelOnlyTextType(100000);
    private TestModelOnlyText _testModelOnlyTextType1000000 = GetTestModelOnlyTextType(1000000);
    private TestModelAllTypes _testModelAllTypesType = GetTestModelAllTypesType();
    private List<TestModelAllTypes> _listTestModelAllTypesType10 = GetListTestModelAllTypesType(10);
    private List<TestModelAllTypes> _listTestModelAllTypesType100 = GetListTestModelAllTypesType(100);
    private List<TestModelAllTypes> _listTestModelAllTypesType1000 = GetListTestModelAllTypesType(1000);

    public async Task<TestModelOnlyText> TestModelOnlyTextType10() => await Task.FromResult(_testModelOnlyTextType10);

    public async Task<TestModelOnlyText> TestModelOnlyTextType1000() => await Task.FromResult(_testModelOnlyTextType1000);

    public async Task<TestModelOnlyText> TestModelOnlyTextType100000() => await Task.FromResult(_testModelOnlyTextType100000);

    public async Task<TestModelOnlyText> TestModelOnlyTextType1000000() => await Task.FromResult(_testModelOnlyTextType1000000);

    public async Task<TestModelAllTypes> TestModelAllTypesType() => await Task.FromResult(_testModelAllTypesType);

    public async Task<List<TestModelAllTypes>> ListTestModelAllTypesType10() => await Task.FromResult(_listTestModelAllTypesType10);

    public async Task<List<TestModelAllTypes>> ListTestModelAllTypesType100() => await Task.FromResult(_listTestModelAllTypesType100);

    public async Task<List<TestModelAllTypes>> ListTestModelAllTypesType1000() => await Task.FromResult(_listTestModelAllTypesType1000);

    public static TestModelOnlyText GetTestModelOnlyTextType(int length)
    {
        return new TestModelOnlyText
        {
            TextField = GetRandomString(length)
        };
    }

    public static TestModelAllTypes GetTestModelAllTypesType()
    {
        return new TestModelAllTypes
        {
            TextField = "0123456789",
            BoolField = true,
            ByteField = 0xFF,
            DateTimeField = DateTime.Now,
            DecimalField = 1.5m,
            DoubleField = 2.5,
            FloatField = 3.5f,
            IntField = 1,
            LongField = 2,
            ShortField = 3
        };
    }

    public static List<TestModelAllTypes> GetListTestModelAllTypesType(int number)
    {
        var result = new List<TestModelAllTypes>();

        for (var i = 0; i < number; i++)
        {
            result.Add(GetTestModelAllTypesType());
        }

        return result;
    }

    private static string GetRandomString(int length)
    {
        var stringBuilder = new StringBuilder();
        var numberOfGuidsToConcat = (((length - 1) / 32) + 1);
        for (var i = 1; i <= numberOfGuidsToConcat; i++)
        {
            stringBuilder.Append(Guid.NewGuid().ToString("N"));
        }

        return stringBuilder.ToString(0, length);
    }
}
