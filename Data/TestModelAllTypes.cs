﻿namespace Data;

public class TestModelAllTypes
{
    public string TextField { get; set; }
    public bool BoolField { get; set; }
    public byte ByteField { get; set; }
    public DateTime DateTimeField { get; set; }
    public decimal DecimalField { get; set; }
    public double DoubleField { get; set; }
    public float FloatField { get; set; }
    public int IntField { get; set; }
    public short ShortField { get; set; }
    public long LongField { get; set; }
}
