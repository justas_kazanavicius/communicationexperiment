﻿namespace Data;

public interface ITestData
{
    Task<TestModelOnlyText> TestModelOnlyTextType10();

    Task<TestModelOnlyText> TestModelOnlyTextType1000();

    Task<TestModelOnlyText> TestModelOnlyTextType100000();

    Task<TestModelOnlyText> TestModelOnlyTextType1000000();

    Task<TestModelAllTypes> TestModelAllTypesType();

    Task<List<TestModelAllTypes>> ListTestModelAllTypesType10();

    Task<List<TestModelAllTypes>> ListTestModelAllTypesType100();

    Task<List<TestModelAllTypes>> ListTestModelAllTypesType1000();
}
